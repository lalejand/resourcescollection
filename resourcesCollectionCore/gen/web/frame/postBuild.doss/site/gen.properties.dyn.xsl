<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:java="http://xml.apache.org/xslt/java" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	exclude-result-prefixes="java">
	<xsl:output encoding="UTF-8" indent="yes" method="text"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="*">destination.dynamicPages.folderPath=<xsl:value-of select="genProp('destination.dynamicPages.folderPath', 'co')"/>
</xsl:template>
</xsl:stylesheet>

