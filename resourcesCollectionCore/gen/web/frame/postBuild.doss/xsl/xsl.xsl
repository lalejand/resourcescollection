<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
	<xsl:output method="text" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

	<xsl:template match="xhtml:body">
		<xsl:text>{</xsl:text>
			<xsl:apply-templates select="//xhtml:a[@sc-download]"/>
		<xsl:text>"":null}</xsl:text>
	</xsl:template>

	<xsl:template match="xhtml:a[@sc-download]">
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@href"/>
		<xsl:text>":"</xsl:text>
		<xsl:value-of select="@sc-download"/>
		<xsl:text>",</xsl:text>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:apply-templates select="@*|node()"/>
	</xsl:template>
		
</xsl:stylesheet>
